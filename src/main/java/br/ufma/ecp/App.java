package br.ufma.ecp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;


public class App 
{

    private static String fromFile() {
        File file = new File("Main.jack");

        byte[] bytes;
        try {
            bytes = Files.readAllBytes(file.toPath());
            String textoDoArquivo = new String(bytes, "UTF-8");
            return textoDoArquivo;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    } 

    public static void main( String[] args )
    {


 

        var input = """
            class SquareGame {
                field Square square; // the square of this game
                field int direction; // the square's current direction: 
                                     // 0=none, 1=up, 2=down, 3=left, 4=right
             
                /** Constructs a new Square Game. */
                constructor SquareGame new() {
                   // Creates a 30 by 30 pixels square and positions it at the top-left
                   // of the screen.
                   let square = Square.new(0, 0, 30);
                   let direction = 0;  // initial state is no movement
                   return this;
                }
             
                /** Disposes this game. */
                method void dispose() {
                   do square.dispose();
                   do Memory.deAlloc(this);
                   return;
                }
             
                /** Moves the square in the current direction. */
                method void moveSquare() {
                   if (direction = 1) { do square.moveUp(); }
                   if (direction = 2) { do square.moveDown(); }
                   if (direction = 3) { do square.moveLeft(); }
                   if (direction = 4) { do square.moveRight(); }
                   do Sys.wait(5);  // delays the next movement
                   return;
                }
             
                /** Runs the game: handles the user's inputs and moves the square accordingly */
                method void run() {
                   var char key;  // the key currently pressed by the user
                   var boolean exit;
                   let exit = false;
                   
                   while (~exit) {
                      // waits for a key to be pressed
                      while (key = 0) {
                         let key = Keyboard.keyPressed();
                         do moveSquare();
                      }
                      if (key = 81)  { let exit = true; }     // q key
                      if (key = 90)  { do square.decSize(); } // z key
                      if (key = 88)  { do square.incSize(); } // x key
                      if (key = 131) { let direction = 1; }   // up arrow
                      if (key = 133) { let direction = 2; }   // down arrow
                      if (key = 130) { let direction = 3; }   // left arrow
                      if (key = 132) { let direction = 4; }   // right arrow
             
                      // waits for the key to be released`
                      while (~(key = 0)) {
                         let key = Keyboard.keyPressed();
                         do moveSquare();
                      }
                  } // while
                  return;
                }
             }
        """
           
        ;
      

        var parser = new Parser(input.getBytes(StandardCharsets.UTF_8));
        parser.parse();
        var result = parser.XMLOutput();
        System.out.println(result);

        /*
        var a = TokenType.RBRACKET;
        System.out.println(a.valueOf);

        
  
     
        var input = fromFile();
        var scanner = new Scanner(input.getBytes(StandardCharsets.UTF_8));
        var result = new StringBuilder();
        
        result.append("<tokens>\r\n");

        for (Token tk = scanner.nextToken(); tk.type !=TokenType.EOF; tk = scanner.nextToken()) {
            result.append(String.format("%s\r\n",tk.toString()));
        }

        result.append("</tokens>\r\n");
        System.out.println(result.toString());
*/
        //String input = "let a[4] = 10 - 5;";
        //String input = "if (10) { let a[4] = 10 - 5; }";
        //String input = "let a = ;";
        //Parser p = new Parser(input.getBytes());
        //p.parse();
       // System.out.println(p.XMLOutput());
        
        //String input = "45 preco2 + 96";


        /*
        Scanner scan = new Scanner(fromFile().getBytes());
        System.out.println("<tokens>");        
        for (Token tk = scan.nextToken(); tk.type != EOF; tk = scan.nextToken()) {
            System.out.println(tk);
        }
        System.out.println("</tokens>");        
        */
    }
}
